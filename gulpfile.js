
var gulp = require('gulp'),
    gzip = require('gulp-gzip'),
    uglify = require('gulp-uglify'),
    webpack = require('webpack'),
    webpack_stream = require('webpack-stream');


gulp.task('webpack', function () {
  gulp.src('./src')
    .pipe(webpack_stream({
      entry:  './src/entry.js',
      output: {
        filename: 'bundle.js',
      },
      watch: true,
      module: {
        loaders: [
          { test: /\.js$/, loaders: ['babel']},
          { test: /\.scss$/, loaders: ['style', 'css', 'sass']}
        ],
      }
    }))
    .pipe(gulp.dest('public/'))
});


gulp.task('build', function() {
  gulp.src('public/bundle.js')
    .pipe(uglify())
    .pipe(gzip())
    .pipe(gulp.dest('public'))
});

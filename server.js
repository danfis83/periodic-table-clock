
var static = require('node-static'),
    moment = require('moment');

var PORT = process.env.PORT || 8080;
var PROD = process.env.NODE_ENV == 'production';

// Create a node-static server instance to serve the './public' folder
var file = new static.Server('./public', {gzip: PROD});

var now = function () {
  return moment().format('YYYY-MM-DD HH:mm:ss');
}

require('http').createServer(function (request, response) {
  request.addListener('end', function () {
    // Serve files
    file.serve(request, response);
  }).resume();

  if (!PROD)
    response.addListener('finish', function () {
      // log requests
      console.log(now(), request.method, request.url, response.statusCode);
    });
}).listen(PORT);

console.log('Started server at ', PORT);

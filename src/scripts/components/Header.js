
import React from 'react';


export class PTCHeader extends React.Component {

  render() {
    return (
      <header>
        <div className="container">
          <h1>Periodic Table Clock</h1>
        </div>
      </header>
    )
  }

}

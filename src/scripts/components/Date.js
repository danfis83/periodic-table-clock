
import React from 'react';

import {PTCElement} from './Element';


export class PTCDate extends React.Component {

  componentWillMount() {
    const date_func = this.setDate.bind(this);
    date_func();
    // TODO: interval
  }

  setDate() {
    this.setState({now: moment()})
  }

  render() {
    return (
      <div className="date">
        <div className="element-group">
          <PTCElement className="year" number={this.state.now.format('YY')} />
          <div className="label">Year</div>
        </div>
        <div className="element-group">
          <PTCElement className="month" number={this.state.now.format('M')} />
          <div className="label">Month</div>
        </div>
        <div className="element-group">
          <PTCElement className="day" number={this.state.now.format('D')} />
          <div className="label">Day</div>
        </div>
      </div>
    )
  }
}

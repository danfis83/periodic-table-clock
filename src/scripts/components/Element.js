
import React from 'react';

import {element_dict} from '../elements'


export class PTCElement extends React.Component {

  render() {
    var element = element_dict[this.props.number];
    if (!element)
      // FIXME
      element = {
        atomic_number: this.props.number,
        name: 'N/A',
        symbol: 'ø'
    }
    return (
      <div className="element">
        <span className="number">{element.atomic_number}</span>
        <span className="symbol">{element.symbol}</span>
        <span className="name">{element.name}</span>
      </div>
    )
  }
}

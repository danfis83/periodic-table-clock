
import React from 'react';

import {PTCElement} from './Element';


export class PTCTime extends React.Component {

  componentWillMount() {
    const time_func = this.setTime.bind(this);
    time_func();
    setInterval(time_func, 1000);
  }

  setTime() {
    this.setState({now: moment()})
  }


  render() {
    return (
      <div className="time">
        <div className="element-group">
          <PTCElement className="hour" number={this.state.now.format('H')} />
          <div className="label">Hour</div>
        </div>
        <div className="element-group">
          <PTCElement className="minute" number={this.state.now.format('m')} />
          <div className="label">Minute</div>
        </div>
        <div className="element-group">
          <PTCElement className="second" number={this.state.now.format('s')} />
          <div className="label">Second</div>
        </div>
      </div>
    )
  }
}

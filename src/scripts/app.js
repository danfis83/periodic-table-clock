
import React from 'react';
import ReactDOM from 'react-dom';

import {PTCDate} from './components/Date';
import {PTCHeader} from './components/Header';
import {PTCTime} from './components/Time';


class PTCApp extends React.Component {

  render() {
    return (
      <div>
        <PTCHeader />
        <div>
          <PTCDate />
        </div>
        <div>
          <PTCTime />
        </div>
      </div>
    )
  }

}

// load app
ReactDOM.render(<PTCApp />, document.getElementById('app'));
